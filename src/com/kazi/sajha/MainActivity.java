package com.kazi.sajha;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	public SimpleLocation location;
	public GPX gps_data;

	TextView VehNumPlate, VehRoute, gpslog, status, locationText, balance;

	Timer timer;
	MyTimerTask myTimerTask;
	Vibrator notify;
	IntentFilter intentFilter;

	SessionManager session;

	String logs = "";
	String vehicleNumber = "", vehicleRoute = "";
	String pushDataUrl = "http://sajha.kazi270.com/getData.php";
	public String batteryLevel;
	public String internetBalance;
	static String androidID;
	boolean GPSinit = false;

	double latAcc = 0;
	double lonAcc = 0;
	protected int interval = 5000; // GPS get data interval. Sending data to the
									// server will be 5 times the rate
	final int balanceCheckInterval = 2160; // ask for balance inquiry in
											// every 3 hours

	final int sendServerRate = 5; // that is 5*interval miliseconds to the
									// server
	final int idleCountLimit = 7;

	protected int intervalCount = 0;
	protected int idleCount = 0;
	protected int gpsInitCount = 0;
	protected int balanceCheckIntervalCount = 0; // ask for balance inquiry in
	// every 3 hours

	List<ArrayList<Double>> gpsArrayList = new ArrayList<ArrayList<Double>>();
	ArrayList<Double> gpsArray = new ArrayList<Double>();

	private BroadcastReceiver intentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// ---display the SMS received in the TextView---
			String balanceInquiry = intent.getExtras().getString("sms");
			int end = balanceInquiry.indexOf("MB");
			int start = balanceInquiry.indexOf("have");
			internetBalance = balanceInquiry.substring(start + 4, end + 2);
			balance.setText(internetBalance);
		}

	};

	private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context arg0, Intent intent) {
			// TODO Auto-generated method stub
			int level = intent.getIntExtra("level", 0);
			batteryLevel = (String.valueOf(level) + "%");
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getIntent().getBooleanExtra("EXIT", false)) {
			finish();
		}
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.main_activity);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		session = new SessionManager(getApplicationContext());
		// construct a new instance of SimpleLocation

		intentFilter = new IntentFilter();
		intentFilter.addAction("SMS_RECEIVED_ACTION");

		session.checkLogin();

		location = new SimpleLocation(this, true, false, interval - 1000);
		gps_data = new GPX();

		notify = (Vibrator) MainActivity.this
				.getSystemService(Context.VIBRATOR_SERVICE);

		// if we can't access the location yet
		if (!location.hasLocationEnabled()) {
			// ask the user to enable location access
			SimpleLocation.openSettings(this);
		}

		VehNumPlate = (TextView) findViewById(R.id.BusNamePlate);
		VehRoute = (TextView) findViewById(R.id.BusRouteInfo);

		gpslog = (TextView) findViewById(R.id.gpsAckInfo);
		locationText = (TextView) findViewById(R.id.locationlabelInfo);
		status = (TextView) findViewById(R.id.devicestatusInfo);
		balance = (TextView) findViewById(R.id.balancelabelInfo);
		HashMap<String, String> user = session.getUserDetails();

		// name
		vehicleNumber = user.get(SessionManager.KEY_NAME);
		vehicleRoute = user.get(SessionManager.KEY_ROUTE);
		TelephonyManager telephonyManager = (TelephonyManager) MainActivity.this
				.getSystemService(Context.TELEPHONY_SERVICE);

		androidID = telephonyManager.getDeviceId().toString();

		this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED));

		location.beginUpdates();
		while (!isInternetOn(MainActivity.this)) {
			status.setText("Error 1"); // No internet connection
		}

		VehNumPlate.setText(vehicleNumber);
		VehRoute.setText(vehicleRoute);
		gpslog.setText("");

		sendSMS("9909", "status");

		if (timer != null) {
			timer.cancel();
		}
		gpsArray.clear();
		gpsArray = new ArrayList<Double>();
		gpsArrayList.clear();
		gpsArrayList = new ArrayList<ArrayList<Double>>();
		timer = new Timer();
		myTimerTask = new MyTimerTask();
		timer.schedule(myTimerTask, 1000, interval);
		Toast.makeText(getBaseContext(), "Process started!", Toast.LENGTH_SHORT)
				.show();

	}

	class MyTimerTask extends TimerTask {

		@Override
		public void run() {

			runOnUiThread(new Runnable() {

				@Override
				public void run() {

					if (gpsInitCount < 6 && GPSinit == false) {

						gpslog.setText("GPS initializing..");
						status.setBackgroundColor(Color.WHITE);
						status.setTextColor(Color.BLACK);
						status.setText((gpsInitCount * 100) / 6 + "%");
						gpsInitCount++;
					} else
						GPSinit = true;

					if (balanceCheckIntervalCount++ >= balanceCheckInterval) {
						balanceCheckIntervalCount = 0;
						sendSMS("9909", "status");
					}

					if (GPSinit) {
						status.setBackgroundColor(Color.RED);
						status.setTextColor(Color.WHITE);
						Date now = new Date();
						String time_ = DateFormat.getTimeInstance(
								DateFormat.MEDIUM).format(now);

						if (location.hasLocationEnabled()) { // Only start the
																// Scheduled
																// task if
																// the location
																// is
																// enabled

							final double accuracy = location.getAccuracy();
							gpslog.setText(time_ + " - "
									+ Double.toString(accuracy));
							gpslog.setTextColor(Color.parseColor("#116611"));

							if (accuracy >= 40) { // more than 50m outer of the
													// radius is not very
													// accurate
								gpslog.setText(time_ + " " + accuracy);
								status.setText("Error 2"); // Weak GPS accuracy,
															// hence data
															// rejected
								latAcc = lonAcc = 0;
								idleCount = 0;
								new HttpAsyncTask().execute(pushDataUrl,
										"Error 2");
								notify.vibrate(500);
								return;
							}

							else {

								if (isInternetOn(MainActivity.this)) { // Only
																		// send
																		// data
																		// if
																		// internet
																		// is
																		// present

									if (!location.LocationChangeState()) {
										idleCount++;
										status.setBackgroundColor(Color.GRAY);
										status.setText("Idle, [" + idleCount
												+ "]"); // Device
														// Idle
										locationText.setText("Waiting..");
										latAcc = lonAcc = 0;
										intervalCount = 0;

									} else {

										latAcc += location.getLatitude();
										lonAcc += location.getLongitude();

										if (Double.toString(location
												.getAccuracy()) != null
												&& Double.toString(location
														.getLatitude()) != null
												&& Double.toString(location
														.getLongitude()) != null) {

											gpsArray = new ArrayList<Double>();
											gpsArray.add(location.getAccuracy());
											gpsArray.add(location.getLatitude());
											gpsArray.add(location
													.getLongitude());
											gpsArrayList.add(intervalCount,
													gpsArray);
											Log.i("gpsarraylist",
													gpsArrayList.toString());
											intervalCount++;
										} else {
											Log.i("Here", "Here");
										}
										status.setText("Collecting..");
										status.setTextColor(Color.WHITE);
										status.setBackgroundColor(Color.GREEN);

									}

									if (idleCount >= idleCountLimit
											|| location.LocationChangeState()) {

										if (intervalCount >= sendServerRate) { // send
																				// data
																				// in
																				// the
																				// interval
											Log.i("intervalCount",
													intervalCount + "");
											intervalCount = 0;
											latAcc /= sendServerRate;
											lonAcc /= sendServerRate;
											Collections
													.sort(gpsArrayList,
															new Comparator<ArrayList<Double>>() {

																@Override
																public int compare(
																		ArrayList<Double> entry1,
																		ArrayList<Double> entry2) {
																	final Double time1 = entry1
																			.get(0);
																	final Double time2 = entry2
																			.get(0);
																	return time1
																			.compareTo(time2);
																}
															}); // Sort in
																// descending
																// roder
																// according to
																// accuract
											Log.i("after sortm",
													gpsArrayList.toString());
											new HttpAsyncTask().execute(
													pushDataUrl, "sucess");
											latAcc = ((gpsArrayList.get(0).get(1) + gpsArrayList.get(1).get(1)) / 2);
											lonAcc = ((gpsArrayList.get(0).get(2) + gpsArrayList.get(1).get(2)) / 2);

											gpsArrayList.clear();
										}

										if (idleCount >= idleCountLimit) {
											new HttpAsyncTask().execute(
													pushDataUrl, "idle");
											Log.i("idle", "device idle");
										}
										idleCount = 0;

										if (location.LocationChangeState())
											location.LocationChangeStateReset();
									}
								} else {
									status.setText("Error 4"); // Internet
																// disabled/unavailable
																// during the
																// process
									new HttpAsyncTask().execute(pushDataUrl,
											"Error 4");
									notify.vibrate(500);
									notify.vibrate(200);
								}
							}
						}

						else {
							status.setText("Error 5"); // Location Disabled
							new HttpAsyncTask().execute(pushDataUrl, "Error 5");
							notify.vibrate(1000);
							return;
						}

					}
				}
			});

		}

	}

	@Override
	protected void onResume() {
		location.beginUpdates();
		registerReceiver(intentReceiver, intentFilter);
		super.onResume();

		// make the device update its location

		// ...
	}

	@Override
	protected void onPause() {
		// stop location updates (saves battery)
		location.endUpdates();
		unregisterReceiver(intentReceiver);

		// ...

		super.onPause();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_HOME)) {
			Toast.makeText(this, "Do not Exit, I repeat do not exit",
					Toast.LENGTH_SHORT).show();
			return false;
		} else
			return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onBackPressed() {
		Toast.makeText(getBaseContext(), "Do not Exit, I repeat do not exit",
				Toast.LENGTH_SHORT).show();
	}

	private class HttpAsyncTask extends AsyncTask<String, Void, String> {
		protected String url = null;
		protected String message = null;

		@Override
		protected String doInBackground(String... urls) {
			url = urls[0];
			message = urls[1];
			gps_data.setStatus(message); // set the status message of the device

			if (message != "sucess") {
				latAcc = location.getLatitude();
				lonAcc = location.getLongitude();
			}
			gps_data.setLat(Double.toString(latAcc)); // push the accumulated //
														// data
			gps_data.setLon(Double.toString(lonAcc));

			gps_data.setSpeed(Double.toString(location.getSpeed()));

			String time_ = java.text.DateFormat.getDateTimeInstance().format(
					Calendar.getInstance().getTime());
			gps_data.setDeviceID(vehicleNumber);
			gps_data.setRoute(vehicleRoute);

			gps_data.setTimeStamp(time_);
			gps_data.setAndroidID(androidID);
			gps_data.setBattLevel(batteryLevel);
			gps_data.setInternetBalance(internetBalance);

			latAcc = lonAcc = 0;

			return POST(url, gps_data); // data collected sucessfully

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {

			Log.i("Response from server", result);
			locationText.setText(gps_data.getLat() + "\n" + gps_data.getLon());
			if (gps_data.getStatus() == "sucess") {
				status.setTextColor(Color.WHITE);
				status.setBackgroundColor(Color.GREEN);
				status.setText("Sent");
			}

		}
	}

	public static String POST(String url, GPX data) {

		String httpResponse = "";
		try {

			// 3. build jsonObject
			List<NameValuePair> GPX_data = new ArrayList<NameValuePair>(2);
			GPX_data.add(new BasicNameValuePair("lat", data.getLat()));
			GPX_data.add(new BasicNameValuePair("status", data.getStatus()));
			GPX_data.add(new BasicNameValuePair("lon", data.getLon()));
			GPX_data.add(new BasicNameValuePair("time_stamp", data
					.getTimeStamp()));
			GPX_data.add(new BasicNameValuePair("device_id", data.getDeviceID())); // change
																					// this
																					// later
																					// to
																					// vehicle_reg
			GPX_data.add(new BasicNameValuePair("speed", data.getSpeed()));
			GPX_data.add(new BasicNameValuePair("route", data.getRoute()));
			GPX_data.add(new BasicNameValuePair("androidID", data
					.getAndroidID())); //
			GPX_data.add(new BasicNameValuePair("battery_level", data
					.getBattLevel()));
			GPX_data.add(new BasicNameValuePair("internet_balance", data
					.getInternetBalance()));

			// 1. create HttpClient
			HttpClient httpclient = new DefaultHttpClient();

			// 2. make POST request to the given URL
			HttpPost httpPost = new HttpPost(url);
			ResponseHandler<String> res_handle = new BasicResponseHandler();
			// 6. set httpPost Entity
			httpPost.setEntity(new UrlEncodedFormEntity(GPX_data));

			// 8. Execute POST request to the given URL
			httpResponse = httpclient.execute(httpPost, res_handle);

			// 10. convert inputstream to string
			if (httpResponse != null) {
				Log.i("Read from server", httpResponse);
				Log.d("Test", "Data sennt to server");
			} else {

				Log.d("Test", "not sent to server");
			}

		} catch (Exception e) {
			Log.d("InputStream", e.getLocalizedMessage());
		}

		// 11. return result
		return httpResponse;
	}

	public static boolean isInternetOn(Context context) {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

	protected void sendSMS(String toPhoneNumber, String smsMessage) {

		try {
			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage(toPhoneNumber, null, smsMessage, null,
					null);
			Toast.makeText(getApplicationContext(), "Balance Inquiry sent.",
					Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "Balance Inquiry  failed.",
					Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

}
